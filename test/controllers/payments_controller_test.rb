require "test_helper"

class PaymentsControllerTest < ActionDispatch::IntegrationTest
  test "should get create_session" do
    get payments_create_session_url
    assert_response :success
  end
end
