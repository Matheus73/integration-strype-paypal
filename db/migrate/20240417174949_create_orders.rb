class CreateOrders < ActiveRecord::Migration[7.1]
  def change
    create_table :orders do |t|
      t.integer :product_id
      t.integer :user_id
      t.string :status
      t.string :token
      t.string :charge_id
      t.string :error_message
      t.string :customer_id
      t.string :payment_gateway

      t.timestamps
    end

    add_monetize :orders, :price, currency: { present: false }
  end
end
