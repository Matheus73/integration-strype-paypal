# frozen_string_literal: true

class CreateProducts < ActiveRecord::Migration[7.1]
  def change
    create_table :products do |t|
      t.string :name
      t.timestamps
    end

    add_monetize :products, :price, currency: { present: true }
  end
end
