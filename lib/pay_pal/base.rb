# frozen_string_literal: true

module PayPal
  # Base class for PayPal requests
  class Base
    def self.call(...)
      new(...).call
    end

    def initialize
      env = PayPalHttp::Environment.new(base_url)
      @http_client = PayPalHttp::HttpClient.new(env)
      puts(client_secret)
      @headers = {
        'content-type' => 'application/json',
        Authorization: "Bearer #{client_secret}"
      }
    end

    def call
      execute
    end

    protected

    def execute
      req = OpenStruct.new({
                             path:,
                             verb:,
                             headers: @headers
                           })

      req.body = body if body.present?

      @http_client.execute(req)
    rescue StandardError => e
      if e.is_a? HttpError
        # Inspect this exception for details
        status_code = e.status_code
      end

    end

    def path
      raise NotImplementedError
    end

    def verb
      'GET'
    end

    def body
      nil
    end

    private

    def client_secret
      ENV['PAY_PAL_CLIENT_SECRET']
    end

    def base_url
      ENV['PAY_PAL_BASE_URL']
    end
  end
end
