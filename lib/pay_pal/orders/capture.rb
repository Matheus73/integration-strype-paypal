# frozen_string_literal: true

module PayPal
  module Orders
    class Capture < Base

      def initialize(order_id)
        @order_id = order_id
        super()
      end

      protected

      def verb
        'POST'
      end

      def path
        "v2/checkout/orders/#{@order_id}/capture"
      end
    end
  end
end
