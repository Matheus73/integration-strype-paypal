# frozen_string_literal: true

module PayPal
  module Orders
    # PayPal::Orders::Create.call(total: '100.00', items:[{name: 'Name product', value: '100.00'}])
    class Create < Base

      def initialize(total:, currency_code: 'USD', items: [])
        return if total.blank? || currency_code.blank? || items.blank?

        super()

        @total = total
        @currency_code = currency_code
        @items = items
      end

      protected

      def path
        'v2/checkout/orders'
      end

      def verb
        'POST'
      end

      def body
        {
          "intent": 'CAPTURE',
          "purchase_units": [
            {
              "items": map_items,
              "amount": {
                "currency_code": @currency_code,
                "value": @total,
                "breakdown": {
                  "item_total": {
                    "currency_code": @currency_code,
                    "value": @total
                  }
                }
              }
            }
          ],
          "application_context": {
            "return_url": 'https://c228-2804-ad8-4522-f400-cd07-8f23-efb4-c0e3.ngrok-free.app/payments/success',
            "cancel_url": 'https://c228-2804-ad8-4522-f400-cd07-8f23-efb4-c0e3.ngrok-free.app/payments/cancel'
          }
        }
      end

      private

      def map_items
        @items.map do |item|
          {
            "name": item[:name],
            "quantity": item[:quantity] || '1',
            "unit_amount": {
              "currency_code": @currency_code,
              "value": item[:value]
            }
          }
        end
      end

    end
  end
end
