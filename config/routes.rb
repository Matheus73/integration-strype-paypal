Rails.application.routes.draw do
  devise_for :users

  resources :products do
    resources :payments, controller: 'products/payments', only: %i[new] do
      collection do
        post 'order_intent' => 'products/payments#order_intent'
        post 'order_paypal' => 'products/payments#order_paypal'
      end
    end
  end

  get 'webhooks/create'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  # root "posts#index"

  resources :payments, only: %i[create new] do
    collection do
      get 'success'
      get 'cancel'
    end
  end

  resources :orders do
    collection do
      post 'capture_order_paypal'
    end
  end

  resources :webhooks, only: :create


  get ".well-known/apple-developer-merchantid-domain-association" => "payments#apple_domain"

end
