import axios from "axios";

const extraHeaders = {}
const csrfToken = document?.querySelector("meta[name='csrf-token']").content

if(csrfToken) extraHeaders["X-CSRF-Token"] = csrfToken


const instance = axios.create({
    headers: {
        "Content-Type": "application/json",
        ...extraHeaders
    }
});

export default instance