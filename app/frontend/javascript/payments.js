import {loadStripe} from '@stripe/stripe-js';
import axios from "../packs/axios";

const productString = document.getElementById('product-id')?.dataset.source
const product = JSON.parse(productString || '{}')
const productId = product.id

async function initializeStripeCheckout (_) {
    const hasStripeCheckout = document.getElementById('express-checkout-element')
    const publicKey = document.getElementById('stripe-key')?.dataset.source


    if (!hasStripeCheckout || !productId) {
        console.error("Not found Stripe")
        return
    }


    const stripe = await loadStripe(publicKey);

    const elements = stripe.elements({
        mode: 'payment',
        currency: product.price_currency.toLowerCase(),
        amount: product.price_cents,
    });

    const expressCheckoutOptions  = {
        wallets:{
            applePay: "always",
            googlePay: "always"
        },
    }
    const expressCheckoutElement = elements.create('expressCheckout', expressCheckoutOptions)

    expressCheckoutElement.mount('#express-checkout-element')

    expressCheckoutElement.on('confirm', async function(event) {
        const clientSecret = await getClientSecret(productId)
        const return_url = `${window.location.origin}/payments/success`

        const {error, paymentIntent} = await stripe.confirmPayment({
            elements,
            clientSecret,
            confirmParams: {return_url},
        })
        console.log("paymentIntent: ", paymentIntent)
        console.log("error: ", error)

    });
}


function initializePaypalCheckout(){
    const env = document.getElementById('paypal-env')?.dataset.source

    paypal.Buttons({
        env, // Valid values are sandbox and live.-->
        style: {
            shape: "rect",
            layout: "vertical",
        },
        async createOrder() {
            const res = await axios.post(`/products/${productId}/payments/order_paypal`)
            return res.data.id
        },
        onApprove: async (data) => {
            const response = await axios.post('/orders/capture_order_paypal', { order_id: data.orderID })

            if (response.data.status === 'COMPLETED') {
                alert('COMPLETED');
                // REDIRECT TO SUCCESS PAGE
            }
        }

    }).render('#paypal-button-container');
}

async function getClientSecret(productId){
    try {
        const res= await axios.post(`/products/${productId}/payments/order_intent`)
        return res.data.token
    } catch (e) {
        console.log(e)
    }

}

initializeStripeCheckout()
initializePaypalCheckout()