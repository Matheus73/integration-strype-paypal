# frozen_string_literal: true

# == Schema Information
#
# Table name: orders
#
#  id              :bigint           not null, primary key
#  error_message   :string
#  payment_gateway :string
#  price_cents     :integer          default(0), not null
#  status          :string
#  token           :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  charge_id       :string
#  customer_id     :string
#  product_id      :integer
#  user_id         :integer
#
class Order < ApplicationRecord
  STATUS_MAP = { pending: 'pending',
                 failed: 'failed',
                 paid: 'paid',
                 paypal_executed: 'paypal_executed',
                 canceled: 'canceled' }.freeze

  PAYMENT_GATEWAY_MAP = { stripe: 'stripe', paypal: 'paypal' }.freeze

  enum :status, STATUS_MAP, suffix: true, default: 'pending'
  enum :payment_gateway, PAYMENT_GATEWAY_MAP, suffix: true

  belongs_to :product
  belongs_to :user
end
