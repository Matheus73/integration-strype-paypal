# frozen_string_literal: true

class OrdersController < ApplicationController
  def capture_order_paypal
    order_result = PayPal::Orders::Capture.call(params[:order_id])

    order = Order.find_by(charge_id: params[:order_id])
    order.status = :paid if order_result.result.status == 'COMPLETED'
    order.save!

    render json: { status: order_result.result.status }, status: :ok
  end
end
