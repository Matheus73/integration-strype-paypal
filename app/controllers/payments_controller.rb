class PaymentsController < ApplicationController
  def new; end

  def create
    Stripe::PaymentIntent.create({
                                   amount: 1099,
                                   currency: 'brl',
                                   payment_method_types: ['card'] })
    token = params[:stripeToken]
    amount = 1000 # Valor em centavos (exemplo: $10.00)

    charge = Stripe::Charge.create(
      amount: amount,
      currency: 'usd',
      description: 'Exemplo de pagamento',
      source: 'tok_visa')

    # Lidar com o sucesso do pagamento
    flash[:success] = "Pagamento realizado com sucesso!"
    redirect_to success_payments_path
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_payment_path
  end

  def success
    # handle successful payments
    # redirect_to root_url, notice: "Purchase Successful"
  end

  def cancel
    # handle if the payment is cancelled
    # redirect_to root_url, notice: "Purchase Unsuccessful"
  end

  def apple_domain
    send_file "#{Rails.root}/app/assets/apple-developer-merchantid-domain-association"
  end

end
