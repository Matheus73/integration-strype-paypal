class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token
  skip_before_action :authenticate_user!


  def create
    payload = request.body.read

    sig_header = request.env['HTTP_STRIPE_SIGNATURE']

    endpoint_secret = ENV['STRIPE_WEBHOOK_ENDPOINT_SECRET']
    event = Stripe::Webhook.construct_event(
      payload, sig_header, endpoint_secret
    )

    case event.type
    when 'payment_intent.succeeded'
      handle_payment_succeeded(event.data.object)
    end

    render status: :created
  end

  private

  def handle_payment_succeeded(payment_intent)
    order = Order.find_by(charge_id: payment_intent.id)
    order.status = :paid
    order.save!

  end
end

