# frozen_string_literal: true

module Products
  class PaymentsController < ApplicationController
    helper_method :product

    def new
      product
    end

    def order_intent
      currency = product.price_currency.downcase
      price_cents = product.price_cents

      order = Order.create!(status: :pending, product:, price_cents:, payment_gateway: :stripe, user: current_user)
      intent_result = Stripe::PaymentIntent.create({
                                                     amount: price_cents,
                                                     currency:,
                                                     automatic_payment_methods: { enabled: true }
                                                   })

      order.charge_id = intent_result.id
      order.token = intent_result.client_secret
      order.save!
      render json: order, status: :created
    end

    def order_paypal
      currency = product.price_currency
      price = product.price.to_s


      result_order = PayPal::Orders::Create.call(total: price,
                                                 currency_code: currency,
                                                 items: [{ name: product.name, value: price }])

      order = Order.create!(status: :pending, product:, price_cents: product.price_cents, payment_gateway: :paypal, user: current_user)
      order.charge_id = result_order.result.id
      order.save!
      render json: result_order.result.table, status: :created
    end

    private

    def product
      product_id = params[:product_id]
      @product ||= Product.find(product_id)
    end
  end
end
